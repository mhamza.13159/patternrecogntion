# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 09:40:29 2019

@author: hamza
"""

# GOODNESS OF FIT 
# USING KOLMOGROV STEST

import scipy as sp
from scipy import stats
from sklearn import datasets
import numpy as np
import pandas as pd
import math
'''

iris = datasets.load_iris()

X = iris.data[:, :2] 


mean = X[:,1].mean()
sd = math.sqrt(np.var(X[:,1]))

sp.stats.kstest(X[:], 'norm', args = (mean, sd), N = 150)
'''
from scipy.stats import gamma


iris = datasets.load_iris() # loading the data set


data1 = iris.data[:, 0] #sepal length 
data2 = iris.data[:, 1] #sepal width
data3 = iris.data[:, 2] #petal length
data4 = iris.data[:, 3] #petal width

x = data2
rv = gamma(3., loc = 0., scale = 2.)

import scipy.stats as ss
from scipy.stats import gamma

from scipy.stats import norm
import matplotlib.pyplot as plt


fig, ax = plt.subplots(1, 1)

a = 1.99323054838
mean, var, skew, kurt = gamma.stats(a, moments='mvsk')
#x = np.linspace(gamma.ppf(0.01, a),gamma.ppf(0.99, a), 100)
ax.plot(x, gamma.pdf(x, a),'r-', lw=5, alpha=0.6, label='gamma pdf')

rv = gamma(a)
ax.plot(x, rv.pdf(x), 'k-', lw=2, label='frozen pdf')
vals = gamma.ppf([0.001, 0.5, 0.999], a)
np.allclose([0.001, 0.5, 0.999], gamma.cdf(vals, a))
r = gamma.rvs(a, size=1000)
ax.hist(x, normed=True, histtype='stepfilled', alpha=0.2)
ax.legend(loc='best', frameon=False)
plt.show()

normdist = getattr(ss, 'norm')

fig, ax1 = plt.subplots(1, 1)

ax1.plot(x, normdist.pdf(x), 'y-', lw=2, label='frozen pdf')
ax1.hist(x, normed=True, histtype='stepfilled', alpha=0.2)
