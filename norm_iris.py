# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 12:33:08 2019

@author: hamza
"""
import numpy as np
import scipy.stats as sp
import matplotlib.pyplot as plt
from sklearn import datasets
import seaborn as sns
import math
from scipy.stats import norm
iris = datasets.load_iris() # loading the data set
from scipy.stats import truncnorm

def get_truncated_normal(mean=0, sd=1, low=0, upp=10):
    return truncnorm(
        (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)

data1 = iris.data[:, 0] #sepal length 
data2 = iris.data[:, 1] #sepal width
data3 = iris.data[:, 2] #petal length
data4 = iris.data[:, 3] #petal width

data = data2 # replace by which ever feature vector you want to fit

# fitted mean and sigma
#mu_f, std_f = norm.fit(data) 

# sample mean and sigman
mu_s = np.mean(data) # sample mean
std_s = math.sqrt(np.var(data)) # sample sigma

#title = "Fit results: mu_f = %.2f,  std_f = %.2f \n sample results: mu_s = %.2f,  std_s = %.2f  " % (mu_f, std_f,mu_s,std_s)
#plt.title(title)

#plt.xlim((-5,5))
#plt.ylim((-1, 1))

normdist = getattr(sp, 'norm')
gammadist = getattr(sp,'gamma')
#param = dist.fit(data)
#plt.scatter(dist.cdf(data),data)
plt.show()
plt.autoscale(True)
#sns.lineplot(data,gammadist.pdf(data2,0.5),c='m')
#sns.lineplot(data,normdist.pdf(data2),c='k')
fig, ax1 = plt.subplots(1, 1)
#ax1.plot(data, normdist.pdf(data), 'k-', lw=1, label='frozen pdf')
#ax1.hist(data, normed=True, histtype='stepfilled', alpha=0.2)
m,s = norm.fit(data)
l = min(data)
u = max(data)
X = get_truncated_normal(mean=m, sd=s, low=l, upp=u)
X = X.rvs(150)
ax1 = sns.distplot(X)
ax1 = sns.distplot(data)

fig, ax2 = plt.subplots(1, 1)
#ax2.plot(data, normdist.cdf(data), 'k-', lw=1, label='Sample cdf')
#ax2.plot(X, normdist.cdf(X), 'm-', lw=1, label='Calculated cdf')
ax2 = sns.lineplot(data,normdist.pdf(data),c='k')
ax2 =sns.lineplot(X,normdist.cdf(X),c='m')

#ax1 = sns.distplot(data)
#ax1 = sns.distplot(norm.cdf(norm.rvs(size=150)))
#sns.lineplot(data,normdist.cdf(data),c='r')
#   sns.lineplot(data,gammadist.cdf(data,0.35),c='b')

#sns.distributions.stats.gaussian_kde(data)
#mu_f, std_f = sp.norm.fit(data) 
#plt.scatter(data,norm.pdf(data))
#plt.scatter(dist.cdf(data),data)
plt.show()
