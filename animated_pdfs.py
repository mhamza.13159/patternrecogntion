# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 09:24:14 2019

@author: hamza
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Nov  7 11:55:12 2019

@author: hamza
"""

import numpy as np
import scipy.stats as sp
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import norm
from scipy.stats import truncnorm
import scipy
import matplotlib.animation as animation


def get_truncated_normal(mean=0, sd=1, low=-100, upp=100):
    return truncnorm(
        (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)


X = get_truncated_normal()
n =1000

# generate some data
dummy = X.rvs(n) # normal dummy data

data = sorted(dummy )
bins =3
fig, ax1 = plt.subplots()

#ax1 =sns.distplot(data)
lw = 0.1
ax1 =sns.lineplot(data,sp.norm.pdf(data),lw=5,c='k',label="True")
RUN =True
plots = []
while RUN:
    if bins==17:
        RUN=False
    y,x= scipy.histogram(data,bins)
    i=0
    p  = []
    xmid= x[:len(x)-1]
    for each in xmid:
        p.append(y[i]/(1000*(xmid[2]-xmid[1])))
        i+=1
    #ax1 = sns.lineplot(xmid,p,legend='brief')
    ax1 = sns.lineplot(x[:len(x)-1],p,legend='brief',lw=lw,label="Estimated From Histogram")
    plots.append(ax1)
    lw+=0.3
    bins=bins+1
    
    #lim=lim-1
Writer = animation.writers['ffmpeg']
writer = Writer(fps=20, metadata=dict(artist='Me'), bitrate=1800)
title = 'PDF using Histogram'
fig = plt.figure()
plt.xlabel('x',fontsize=20)
plt.ylabel('PDFs',fontsize=20)
plt.title(title,fontsize=20)

def animate(i,lw=1):

    y,x= scipy.histogram(data,bins)
    i=0
    p  = []
    xmid= x[:len(x)-1]
    for each in xmid:
        p.append(y[i]/(1000*(xmid[2]-xmid[1])))
        i+=1
    #ax1 = sns.lineplot(xmid,p,legend='brief')
    ax1 = sns.lineplot(x[:15],p,legend='brief',lw=lw,label="Estimated From Histogram")
    plots.append(ax1)
    #lw+=0.3
    #bins=bins+1
       
ani = animation.FuncAnimation(fig, animate(15), frames=200, repeat=True)
'''
y,x= scipy.histogram(data,bins)
i=0
p  = []
xmid= (x[1:] + x[:-1]) / 2
for each in xmid:
    p.append(y[i]/(1000*(xmid[2]-xmid[1])))
    i+=1
#ax1 = sns.lineplot(xmid,p,legend='brief')
ax1 = sns.lineplot(x[:len(x)-1],p,legend='brief')



'''