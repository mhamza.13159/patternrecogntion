# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 17:15:04 2019

@author: hamza
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 21:37:55 2019

@author: hamza
"""

    
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sklearn as sk
from sklearn import datasets
import numpy as np
import pandas as pd
# import some data to play with
iris = datasets.load_iris()


#Features information
'''
iris.feature_names 
Out[14]: 
['sepal length (cm)',
 'sepal width (cm)',
 'petal length (cm)',
 'petal width (cm)']
'''
# Target class information
'''
iris.target_names
Out[15]: array(['setosa', 'versicolor', 'virginica'], dtype='<U10')
'''

X = iris.data[:, :] 

# target class 
y = iris.target

df = pd.DataFrame()

#set data frame df columns' names

# sepal length
df['slength'] = X[:,0]

# sepal width
df['swidth'] = X[:,1]

# petal length  
df['plength'] = X[:,2]

# sepal width
df['pwidth'] = X[:,3]


df['target']= y


# Divide the data frame  df class wise for exploratory purpose
setosa = df[df['target']==0]
versicolor = df[df['target']==1]
virginica = df[df['target']==2]


fig1 = plt.figure()
fig2 = plt.figure()
fig3 = plt.figure()
fig4 = plt.figure()

ax1 = fig1.add_subplot(111)
ax2 = fig2.add_subplot(111)
ax3 = fig3.add_subplot(111)
ax4 = fig4.add_subplot(111)
#ax1.set_xlabel(xlabel='target')
#ax1.set_ylabel(ylabel='length/width')

#sepal

#   length
ax1.scatter(setosa['target'],setosa['slength'],color='r')
ax1.scatter(versicolor['target'],versicolor['slength'],color='g')
ax1.scatter(virginica['target'],virginica['slength'],color='b')

#   width
ax2.scatter(setosa['target'],setosa['swidth'],color='r')
ax2.scatter(versicolor['target'],versicolor['swidth'],color='g')
ax2.scatter(virginica['target'],virginica['swidth'],color='b')

#petal

#   length
ax3.scatter(setosa['target'],setosa['plength'],color='r')
ax3.scatter(versicolor['target'],versicolor['plength'],color='g')
ax3.scatter(virginica['target'],virginica['plength'],color='b')

#   width
ax4.scatter(setosa['target'],setosa['pwidth'],color='r')
ax4.scatter(versicolor['target'],versicolor['pwidth'],color='g')
ax4.scatter(virginica['target'],virginica['pwidth'],color='b')



ax1.set_title('Sepal Length Wise')
ax1.hist(setosa.slength,color='r',fill=False,edgecolor='r',stacked=False)
ax1.hist(versicolor.slength,color='g',fill=False,edgecolor='g')
ax1.hist(virginica.slength,color='b',fill=False,edgecolor='b')

ax2.set_title('Sepal Width Wise')
ax2.hist(setosa.swidth,color='r',fill=False,edgecolor='r',stacked=False)
ax2.hist(versicolor.swidth,color='g',fill=False,edgecolor='g')
ax2.hist(virginica.swidth,color='b',fill=False,edgecolor='b')

ax3.set_title('Petal Length Wise')

ax3.hist(setosa.plength,color='r',fill=False,edgecolor='r',stacked=False)
ax3.hist(versicolor.plength,color='g',fill=False,edgecolor='g')
ax3.hist(virginica.plength,color='b',fill=False,edgecolor='b')

ax4.set_title('Petal Width Wise')
ax4.hist(setosa.pwidth,color='r',fill=False,edgecolor='r',stacked=False)
ax4.hist(versicolor.pwidth,color='g',fill=False,edgecolor='g')
ax4.hist(virginica.pwidth,color='b',fill=False,edgecolor='b')



#ENOUGH OF VIZ

#GET TO THE train test split 

# generate random 150 integers 
idx = np.random.permutation(150)

# random sampling sets split 70 30 train test ratio
idxtrain = idx[0:round(0.7*150)]
idxtest = idx[round(0.7*150)+1:]


#train set
trs = df.iloc[idxtrain]

#test set
ts = df.iloc[idxtest]

#sepal

#ts_s = np.array([(ts.slength),(ts.swidth)] ).transpose()
#trs_s = np.array([(trs.slength),(trs.swidth)] ).transpose()

#petal

ts_s = np.array([(ts.plength),(ts.pwidth)] ).transpose()
trs_s = np.array([(trs.plength),(trs.pwidth)] ).transpose()



#[trs.slength,trs.swidth]
res = sk.metrics.pairwise_distances_argmin_min(ts_s,trs_s)

result = pd.DataFrame()
result = ts
result['predicted_index'] = np.array(trs.target.iloc[res[0]])

confusion_mat =sk.metrics.confusion_matrix(result.target,result.predicted_index)
#result['min_distance'] = res[1]










'''
#sepal
#   length
ax1.scatter(setosa['target'],setosa['slength'],1,color='r')
ax1.scatter(versicolor['target'],versicolor['slength'],1,color='g')
ax1.scatter(virginica['target'],virginica['slength'],1,color='b')

#   width
ax1.scatter(setosa['target'],setosa['swidth'],1,color='c')
ax1.scatter(versicolor['target'],versicolor['swidth'],1,color='m')
ax1.scatter(virginica['target'],virginica['swidth'],1,color='k')

countdf =  pd.DataFrame(setosa.plength.value_counts())

#test = setosa.groupby('plength')['plength']

ax1.set_title('Petal Length Wise')
ax1.hist(setosa.plength,color='r',fill=False,edgecolor='r',stacked=False)
ax1.hist(versicolor.plength,color='g',fill=False,edgecolor='g')
ax1.hist(virginica.plength,color='b',fill=False,edgecolor='b')

#ax1.set_title('Petal Length Wise')

ax1.hist(setosa.pwidth,color='r',fill=False,edgecolor='k',stacked=False)
ax1.hist(versicolor.pwidth,color='g',fill=False,edgecolor='c')
ax1.hist(virginica.pwidth,color='b',fill=False,edgecolor='m')

#ax1.bar(setosa.groupby('plength')['plength'].nunique(),setosa.plength.value_counts(),0)


ax2.set_title('Sepal Length/width Wise')
ax2.hist(setosa.slength,color='r',fill=False,edgecolor='r')
ax2.hist(versicolor.slength,color='g',fill=False,edgecolor='g')
ax2.hist(virginica.slength,color='b',fill=False,edgecolor='b')


ax2.hist(setosa.swidth,color='r',fill=False,edgecolor='k',stacked=False)
ax2.hist(versicolor.swidth,color='g',fill=False,edgecolor='c')
ax2.hist(virginica.swidth,color='b',fill=False,edgecolor='m')

'''