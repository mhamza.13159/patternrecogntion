# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 20:49:13 2019

@author: hamza
"""


import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sklearn as sk
from sklearn import datasets
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd


# import iris data
iris = datasets.load_iris()

# iris data into pandas dataframe
irisdf = pd.DataFrame(iris.data)

# give col names as sepal , petal  length/width
irisdf.columns = iris.feature_names 

# class data frame

irisdf = pd.DataFrame(iris.data)
irisdf.columns = iris.feature_names
irisdf['target'] = iris.target


gp0 = irisdf[irisdf.target==0]

fig1, ax1 = plt.subplots()



