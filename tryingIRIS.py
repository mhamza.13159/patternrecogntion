# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 21:37:55 2019

@author: hamza
"""

    
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sklearn as sk
from sklearn import datasets
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd
# import some data to play with
iris = datasets.load_iris()


#Features information
'''
iris.feature_names 
Out[14]: 
['sepal length (cm)',
 'sepal width (cm)',
 'petal length (cm)',
 'petal width (cm)']
'''
# Target class information
'''
iris.target_names
Out[15]: array(['setosa', 'versicolor', 'virginica'], dtype='<U10')
'''

# we only take the first two features; the sepal.
X = iris.data[:, :2] 

# target class 
y = iris.target

df = pd.DataFrame()

# sepal length
df['slength'] = X[:,0]

# sepal width
df['swidth'] = X[:,1]


df['target']= y

setosa = df[df['target']==0]
versicolor = df[df['target']==1]
virginica = df[df['target']==2]



swidth = df.swidth
slength = df.slength

# unique variety of lengths
#nonunique=df.groupby('swidth')['swidth'].nunique()

#nonunique= pd.DataFrame(nonunique)

# count of each occurance of length/width

#count = df.swidth.value_counts()
count = df.slength.value_counts()

countdf = pd.DataFrame(count)
#countdf = countdf.rename(columns={"swidth": "count",})
countdf = countdf.rename(columns={"slength": "count",})

#countdf['swidth'] = countdf.index
countdf['slength'] = countdf.index
#countdf['class'] = []
countdf=countdf.merge(df)

gp0 = countdf[countdf['target']==0]

gp1 = countdf[countdf['target']==1]

gp2 = countdf[countdf['target']==2]

fig1 = plt.figure()
fig2 = plt.figure()
#fig3 = plt.figure()

ax1 = fig1.add_subplot(111, projection='3d')
ax2 = fig2.add_subplot(111, projection='3d')
#ax3 = fig3.add_subplot(111, projection='3d')


'''
ax1.set_xlabel(xlabel='count', size=10)
ax1.set_ylabel(ylabel='Target' , color='b', size=10)
ax1.set_zlabel(zlabel='width')


ax2.set_xlabel(xlabel='count', size=10)
ax2.set_ylabel(ylabel='Target' , color='b', size=10)
ax2.set_zlabel(zlabel='Width cm')
'''
ax1.set_xlabel(xlabel='count', size=10)
ax1.set_ylabel(ylabel='Target' , color='b', size=10)
ax1.set_zlabel(zlabel='length')


ax2.set_xlabel(xlabel='length', size=10)
ax2.set_ylabel(ylabel='Target' , color='b', size=10)
ax2.set_zlabel(zlabel='count cm')

'''
ax3.set_xlabel(xlabel='Width cm', size=10)
ax3.set_ylabel(ylabel='Count' , color='b', size=10)
ax3.set_zlabel(zlabel='Class')
'''

ax1.scatter(gp0['count'],gp0['target'],gp0['slength'],color='r')
ax1.scatter(gp1['count'],gp1['target'],gp1['slength'],color='g')
ax1.scatter(gp2['count'],gp2['target'],gp2['slength'],color='b')


ax2.plot(gp0['slength'],gp0['target'],gp0['count'],color='r')
ax2.plot(gp1['slength'],gp1['target'],gp1['count'],color='g')
ax2.plot(gp2['slength'],gp2['target'],gp2['count'],color='b')

#ax2.plot(gp1['count'],gp1['target'],gp1['slength'],color='g')
#ax2.plot(gp2['count'],gp2['target'],gp2['slength'],color='b')

ax1.scatter(gp1['swidth'],gp1['class'],gp1['count'],color='b')
ax1.scatter(gp2['swidth'],gp2['class'],gp2['count'],color='g')

#ax1.bar(gp0['swidth'],gp0['count'],fill=True,width=0.05,linewidth=0.2,align='center',edgecolor ='r')
#ax2.bar(gp1['swidth'],gp1['count'],fill=True,edgecolor = 'g',width=0.1,linewidth=0.2)
#ax3.bar(gp2['swidth'],gp2['count'],fill=True,edgecolor = 'b',width=0.1,linewidth=0.2)

#plt.show(True)
'''
slength = df.slength
# unique variety of lengths
nonunique=df.groupby('slength')['slength'].nunique()

nonunique= pd.DataFrame(nonunique)

# count of each occurance of length
count = df.slength.value_counts()
countdf = pd.DataFrame(count)
countdf = countdf.rename(columns={"slength": "count",})
countdf['slength'] = countdf.index
#countdf['class'] = []
countdf=countdf.merge(df)

gp0 = countdf[countdf['class']==0]

gp1 = countdf[countdf['class']==1]

gp2 = countdf[countdf['class']==2]



plt.bar(gp0['slength'],gp0['count'],fill=False,edgecolor = 'r',width=0.1)

plt.bar(gp1['slength'],gp1['count'],fill=False,edgecolor = 'g',width=0.1)
plt.bar(gp2['slength'],gp2['count'],fill=False,edgecolor = 'b',width=0.1)
'''

#plt.bar(gp1['slength'],gp1['count'], fill=False,color = 'g')
#plt.bar(gp2['slength'],gp2['count'], fill=False,color = 'b')

#countdf['class'] = df['class']


#plt.bar(countdf.index, countdf.slength,fill=False,width=0.09,)

'''
x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
'''



# Plot the training points
'''
plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Set1,
            edgecolor='k')
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')
'''
'''
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.xticks(())
plt.yticks(())
'''

# To getter a better understanding of interaction of the dimensions
# plot the first three PCA dimensions
'''
fig = plt.figure(1, figsize=(8, 6))
ax1 = Axes3D(fig, elev=-150, azim=110)
X_reduced = PCA(n_components=3).fit_transform(iris.data)
ax1.scatter(X_reduced[0:49, 0], X_reduced[0:49, 1], X_reduced[0:49, 2], c=y[0:49],
           cmap=plt.cm.Set1, edgecolor='blue', s=40)
ax1.scatter(X_reduced[50:100, 0], X_reduced[50:100, 1], X_reduced[50:100, 2], c=y[50:100],
           cmap=plt.cm.Set1, edgecolor='red', s=40)
ax1.scatter(X_reduced[101:150, 0], X_reduced[101:150, 1], X_reduced[101:150, 2], c=y[101:150],
           cmap=plt.cm.Set1, edgecolor='yellow', s=40)
'''
# how is above supposed to tell us someting ?? a lil confused 

'''
# generate random 150 integers 
idx = np.random.permutation(150)

# random sampling sets split 70 30 train test ratio
idxtrain = idx[0:round(0.7*150)]
idxtest = idx[round(0.7*150)+1:]

# train/test sets
trainset = X[idxtrain,:]
testset = X[idxtest,:]

#labels
trainlabel = iris.target[idxtrain]
testlabel = iris.target[idxtest]


# probabilities
P_setosa = sum(trainlabel==0)/105
P_versicolor= sum(trainlabel==1)/105
P_virginica = sum(trainlabel==2)/105

# means 
m_setosa = np.mean(trainset[(trainlabel==0),:])
m_versicolor = np.mean(trainset[(trainlabel==1),:])
m_virginica = np.mean(trainset[(trainlabel==2),:])

#Covariances 
c_setosa = np.cov(trainset[(trainlabel==0),:])
c_versicolor = np.cov(trainset[(trainlabel==1),:])
c_virginica = np.cov(trainset[(trainlabel==2),:])

m = [m_setosa,m_versicolor,m_virginica]

def min_dist(m1,m2,m3,ts):
    return 0

'''



        