# -*- coding: utf-8 -*-
"""
Created on Thu Nov  7 11:55:12 2019

@author: hamza
"""

import numpy as np
import scipy.stats as sp
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import norm
from scipy.stats import truncnorm
import scipy


def get_truncated_normal(mean=0, sd=1, low=-100, upp=100):
    return truncnorm(
        (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)


X = get_truncated_normal()
n =1000

# generate some data
dummy = X.rvs(n) # normal dummy data

data = sorted(dummy )
bins =3
fig, ax1 = plt.subplots()

#ax1 =sns.distplot(data)
lw = 0.1
ax1 =sns.lineplot(data,sp.norm.pdf(data),lw=5,c='k',label="True")

# all other graphs
ax1 =sns.lineplot(data,sp.norm.pdf(data),lw=0.2,c='m',label=" Rest Estimated")

RUN =True
title = "PDF Estimation using Histogram Method"
xlabel = "data"
ylabel = "pdfs"

plt.xlabel(xlabel,fontsize=10)
plt.ylabel(ylabel,fontsize=10)
plt.title(title,fontsize=15)
while RUN:
    # stoping criteria 
    if bins==17:
        RUN=False
    y,x= scipy.histogram(data,bins)
    i=0
    #probability density using p(x) = k/nV
    p  = []
    xmid= x[:len(x)-1]
    for each in xmid:
        p.append(y[i]/(1000*(xmid[2]-xmid[1])))
        i+=1
    #ax1 = sns.lineplot(xmid,p,legend='brief')
    ax1 = sns.lineplot(x[:len(x)-1],p,legend='brief',lw=lw)
    lw+=0.3
    bins=bins+1
    
    #lim=lim-1

