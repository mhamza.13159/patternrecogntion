# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 13:20:42 2019

@author: hamza
"""

import numpy as np
import scipy.stats as sp
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import norm
from scipy.stats import truncnorm
import scipy
import pandas as pd
import random
from scipy.spatial import distance
from sklearn import datasets

iris = datasets.load_iris()

#without labels
irisdf = pd.DataFrame(iris.data)

#lets try clustering and validate with both internal and extranal verifications

covar = irisdf.cov() #covariance
correl = irisdf.corr() #correlation
mean = irisdf.mean() # means

data1 = iris.data[:, 0] #sepal length 
data2 = iris.data[:, 1] #sepal width
data3 = iris.data[:, 2] #petal length
data4 = iris.data[:, 3] #petal width

#irisdf = irisdf.drop(columns=[3])

# we can see the clarity of all 4 features here : 0(sepal length) and petal length seem to give most information
sns.scatterplot(data=irisdf)

#lets first try clustring with 1 to 10 number of cluster and check for convergnece
limit = 10
WSS = []
BSS = []

#centroids generator 
def centroidGenerator(n,lbound,ubound):
    start =1
    centroids= []
    while(start<=n):
        centroid = (random.uniform(lbound,ubound),random.uniform(lbound,ubound),random.uniform(lbound,ubound),random.uniform(lbound,ubound))
        centroids.append(centroid)
        start+=1
    return centroids

i=2
while (i<=limit):
    centroids =centroidGenerator(i,min(mean),max(mean))
    
    i+=1
dists = []
#for centroid in centroids:
dist = []        

    
labels=[]
rowNum = 0
irisdf["labels"] = iris.target
irisdf["labels_pred"] = np.arange(10,160,1)
for row in (irisdf.iloc[:,:4]).iterrows():
    #print("Centroid 1 ",centroids[1])
    #print("Data point",row[1])
    #print(matrow[1]-centroids[1])
    minCentroid="other"
    minDist=99999999
    centroidcount=1
    for centroid in centroids:
        d = distance.euclidean(centroid,row[1])
        
        irisdf[""+str(centroidcount-1)] = d
        if(d<minDist):
            minDist=d
            minCentroid=str(centroidcount-1)
            irisdf.labels_pred.iloc[rowNum]=minCentroid
            centroi = irisdf[irisdf['labels_pred']==minCentroid]
            print(len(centroi))
            x = np.vectorize((centroi.iloc[:,:4]).mean())
            y = np.vectorize(centroid)
            newcentroid = (x.pyfunc+y.pyfunc)/2
            centroids[centroidcount-1]= newcentroid
            #print((centroi.iloc[:,:4]).mean())
            #centroids[centroidcount] = (centroi.mean + centroid)/2
            
        centroidcount+=1
        #print(d)
    labels.append(minCentroid)
    rowNum+=1
    #print(d)
#   dist.append(d)
print(irisdf.labels_pred.value_counts())



'''
dists.append(dist)

centroidcount=1
for dist in dists:
    irisdf["centroid_"+str(centroidcount)] = dist
    centroidcount+=1

labels = []
for row in (irisdf.iloc[:,4:]).iterrows():
    labels.append(pd.core.series.Series.idxmin(row[1]))

labelsdf = pd.DataFrame()
labelsdf["labels"] = labels
print(labelsdf.labels.value_counts())
irisdf["labels"]=labels
'''
gp0 = irisdf[irisdf.labels_pred=="0"].iloc[:,:4]

gp1 = irisdf[irisdf.labels_pred=="1"].iloc[:,:4]

gp2 = irisdf[irisdf.labels_pred=="3"].iloc[:,:4]

gp3 = irisdf[irisdf.labels_pred=="8"].iloc[:,:4]



'''
while (i<150):
    print(len(dist_matrix[i+1:,i]))
    i+=1
'''
'''
import mpl_toolkits.mplot3d.axes3d as p3
fig = plt.figure()
ax = p3.Axes3D(fig)
ax.view_init(7, -80)
ax.plot3D(gp0, gp1,gp2,'o')
'''