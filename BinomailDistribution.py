# -*- coding: utf-8 -*-
"""
Created on Sun Oct  6 12:53:28 2019

@author: hamza
"""

import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
from decimal import Decimal


# implementation of 
def myPDF_plotter(Numberoftrials,parameter):
    N = Numberoftrials
    i=0
    x = np.arange(0,N+1,1)
    parameter = parameter
    fx=[]
    while i<=N:
        factorial_trials = Decimal(math.factorial(N))
        nth_trial_factorial= Decimal(math.factorial(i))
        N_minus_nth_trial_factorial = Decimal(math.factorial(N-i))
        parameter_raise_to_the_pow_nth_trial = Decimal(math.pow(parameter,i))
        one_minus_parameter_raise_to_the_pow_N_minus_nth_trial = Decimal(math.pow((1-parameter),(N-i)))
        fx.append((factorial_trials*parameter_raise_to_the_pow_nth_trial*one_minus_parameter_raise_to_the_pow_N_minus_nth_trial)/(nth_trial_factorial*N_minus_nth_trial_factorial))
        #fx.append((math.factorial(N)*math.pow(parameter,i)*math.pow((1-parameter),(N-i)))/((math.factorial(i))*(math.factorial(N-i))))
        i+=1
    plt.xlabel= 'data'
    plt.ylabel= 'f(data|parameter)'
    plt.plot(x,fx,c='r')
    plt.bar(x,fx,width=1)


myPDF_plotter(10,0.7)
