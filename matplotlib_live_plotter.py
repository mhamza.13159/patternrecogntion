# -*- coding: utf-8 -*-
"""
Created on Fri Oct  4 22:49:04 2019

@author: hamza
"""

#import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)
#t = np.arange(0.0, 1.0, 0.001)
n = 1
inversen = 1/n
#delta_f = 1
#s = a0 * np.sin(2 * np.pi * f0 * t)
#s = 1/n
i =0

l, = plt.plot( inversen)
ax.margins(x=0)

axcolor = 'lightgoldenrodyellow'
axfreq = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
#axamp = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)

sfreq = Slider(axfreq, 'n', 1, 100, valinit=n, valstep=1)
#samp = Slider(axamp, 'Amp', 0.1, 10.0, valinit=a0)


def update(val):
#    amp = samp.val
    N = sfreq.val
    l.set_ydata(1/N)
    fig.canvas.draw_idle()


sfreq.on_changed(update)
#samp.on_changed(update)
plt.show()

