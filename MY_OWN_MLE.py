
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sklearn as sk
from sklearn import datasets
from sklearn.decomposition import PCA
import numpy as np
import seaborn as sb
import math
from scipy.stats import norm
# import some data to play with
iris = datasets.load_iris()



#Features information
'''
iris.feature_names 
Out[14]: 
['sepal length (cm)',
 'sepal width (cm)',
 'petal length (cm)',
 'petal width (cm)']
'''
# Target class information
'''
iris.target_names
Out[15]: array(['setosa', 'versicolor', 'virginica'], dtype='<U10')
'''

# we only take the 3rd feature; the petal length .
X = iris.data[:, 3] 

y = iris.target

'''
x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
'''



# Plot the training points
'''
plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Set1,
            edgecolor='k')
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')
'''
'''
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.xticks(())
plt.yticks(())
'''

# To getter a better understanding of interaction of the dimensions
# plot the first three PCA dimensions
'''
fig = plt.figure(1, figsize=(8, 6))
ax1 = Axes3D(fig, elev=-150, azim=110)
X_reduced = PCA(n_components=3).fit_transform(iris.data)
ax1.scatter(X_reduced[0:49, 0], X_reduced[0:49, 1], X_reduced[0:49, 2], c=y[0:49],
           cmap=plt.cm.Set1, edgecolor='blue', s=40)
ax1.scatter(X_reduced[50:100, 0], X_reduced[50:100, 1], X_reduced[50:100, 2], c=y[50:100],
           cmap=plt.cm.Set1, edgecolor='red', s=40)
ax1.scatter(X_reduced[101:150, 0], X_reduced[101:150, 1], X_reduced[101:150, 2], c=y[101:150],
           cmap=plt.cm.Set1, edgecolor='yellow', s=40)
'''
# how is above supposed to tell us someting ?? a lil confused 


# generate random 150 integers 
idx = np.random.permutation(150)

# random sampling sets split 70 30 train test ratio
idxtrain = idx[0:round(0.7*150)]
idxtest = idx[round(0.7*150)+1:]

# train/test sets
trainset = X[idxtrain]
testset = X[idxtest]

#labels
trainlabel = iris.target[idxtrain]
testlabel = iris.target[idxtest]

'''
# probabilities
P_setosa = sum(trainlabel==0)/105
P_versicolor= sum(trainlabel==1)/105
P_virginica = sum(trainlabel==2)/105

# means 

m_setosa = np.mean(trainset[(trainlabel==0),:])
m_versicolor = np.mean(trainset[(trainlabel==1),:])
m_virginica = np.mean(trainset[(trainlabel==2),:])

#Covariances 
c_setosa = np.cov(trainset[(trainlabel==0),:])
c_versicolor = np.cov(trainset[(trainlabel==1),:])
c_virginica = np.cov(trainset[(trainlabel==2),:])

m = [m_setosa,m_versicolor,m_virginica]
'''

mu_sample = np.mean(trainset)
std_sample = math.sqrt(np.var(trainset))
l_bound = min(trainset)
u_bound = max(trainset)
z1 = (l_bound-mu_sample)/std_sample
z2 = (u_bound-mu_sample)/std_sample

x = np.arange(l_bound, u_bound, 0.001)
x_all = X
y = norm.pdf(x,mu_sample,std_sample)
y2 = norm.pdf(x_all,mu_sample,std_sample)
# build the plot
fig, ax = plt.subplots(figsize=(9,6))
plt.style.use('fivethirtyeight')
ax.plot(x_all,y2)

ax.fill_between(x,y,0, alpha=0.3, color='b')
ax.fill_between(x_all,y2,0, alpha=0.1)
ax.set_xlim([-10,10])
ax.set_xlabel('# of Standard Deviations Outside the Mean')
ax.set_yticklabels([])
ax.set_title('Normal Gaussian Curve')

plt.savefig('normal_curve.png', dpi=72, bbox_inches='tight')
plt.show()

