# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 08:42:54 2019

@author: hamza
"""

# MLE Maximum likelihood estimation

# import libraries
import numpy as np, pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.optimize import minimize
import scipy.stats as stats
import pymc3 as pm3
import numdifftools as ndt
import statsmodels.api as sm
from statsmodels.base.model import GenericLikelihoodModel

# generate data
N = 100
x = np.linspace(0,20,N)
ϵ = np.random.normal(loc = 0.0, scale = 5.0, size = N)
y = (3*x) + ϵ
df = pd.DataFrame({'y':y, 'x':x})
df['constant'] = 1

# plot
sns.regplot(df.x, df.y);

X = df[['constant', 'x']]

sm.OLS(y,X).fit().summary()

# define likelihood function
def MLERegression(params):
 intercept, beta, sd = params[0], params[1], params[2] # inputs are guesses at our parameters
 yhat = intercept + beta*x # predictions# next, we flip the Bayesian question
# compute PDF of observed values normally distributed around mean (yhat)
# with a standard deviation of sd
 negLL = -np.sum( stats.norm.logpdf(y, loc=yhat, scale=sd) )# return negative LL
 return(negLL)
 
 # let’s start with some random coefficient guesses and optimize
guess = np.array([5,5,2])
results = minimize(MLERegression, guess, method = 'Nelder-Mead',options={'disp': True})