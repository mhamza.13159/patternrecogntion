# -*- coding: utf-8 -*-
"""
Created on Sun Oct  6 16:43:17 2019

@author: hamza
"""

import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt
from decimal import Decimal
import time


start = time.time()
# implementation of MLE Estimation


N= 100 #number_of_trials

parameter = np.arange(0,1+1/N,1/N)
dp = 50 #data point

nf = Decimal(math.factorial(N)) #number_of_trials factorail N!

dpf = Decimal(math.factorial(dp)) #data point factorial dp!

N_minus_dp_f = Decimal(math.factorial(N-dp)) # (N -dp)! 

df= pd.DataFrame()
df['para'] = parameter

likelihood = []
i = 0

'''
while i<=N:
    parameter_raise_to_the_pow_dp = Decimal(math.pow(parameter[i],dp))
    one_minus_parameter_raise_to_the_pow_N_minus_dp = Decimal(math.pow(1-parameter[i],N-dp))
    likelihood.append((nf*parameter_raise_to_the_pow_dp*one_minus_parameter_raise_to_the_pow_N_minus_dp)/(dpf*N_minus_dp_f))
    i+=1
df['likelihood']= likelihood
#print(max(df.likelihood))
max_L_idx  = df[df['likelihood']==max(df['likelihood'])].index.values.astype(int)[0]
#plt.scatter(df.para.iloc[max_L_idx],max(df.likelihood), c='r',)
fig, ax = plt.subplots()
ax.plot(df.para.iloc[max_L_idx],max(df.likelihood),'ro',)
#label = str(float(df.para.iloc[max_L_idx]))+","+str(float(max(df.likelihood)))
rd1 = round(float(max(df.likelihood)),2)
rd2 = round(df.para.iloc[max_L_idx],2)
label = "  "+str(rd2) +","+str(rd1)
print(label)
ax.annotate(label,(df.para.iloc[max_L_idx],rd1 ))
ax.plot(parameter,likelihood)

# converting above into log(L(paramater|data))
'''
#'''

while i<=N:
    #print(i)
    parameter_raise_to_the_pow_dp = Decimal(math.pow(parameter[i],dp))
    one_minus_parameter_raise_to_the_pow_N_minus_dp = Decimal(math.pow(1-parameter[i],N-dp))
    #likelihood.append(math.log((nf*parameter_raise_to_the_pow_dp*one_minus_parameter_raise_to_the_pow_N_minus_dp)/(dpf*N_minus_dp_f)))
    
    #try:
        #likelihood.append(math.log(nf/(dpf*N_minus_dp_f))+dp*math.log(parameter[i])+(N-dp)*math.log(1-parameter[i]))
    likelihood.append(dp/parameter[i]-(N-dp)/(1-parameter[i]))
    #except ValueError as e:
    #    print(e)
    #    likelihood.append(0)
        #i+=1
    i+=1
df['likelihood']= likelihood
#print(max(df.likelihood))
max_L_idx  = df[df['likelihood']==max(df['likelihood'])].index.values.astype(int)[0]
#plt.scatter(df.para.iloc[max_L_idx],max(df.likelihood), c='r',)
fig1, ax1 = plt.subplots()
ax1.plot(df.para.iloc[max_L_idx],max(df.likelihood),'ro',)
#label = str(float(df.para.iloc[max_L_idx]))+","+str(float(max(df.likelihood)))
rd1 = round(float(max(df.likelihood)),2)
rd2 = round(df.para.iloc[max_L_idx],2)
label = "  "+str(rd2) +","+str(rd1)
print(label)
ax1.annotate(label,(df.para.iloc[max_L_idx],rd1 ))
ax1.plot(parameter,likelihood)

#'''

end = time.time()

print(end-start, N)