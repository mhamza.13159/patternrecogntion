# -*- coding: utf-8 -*-
"""
Created on Wed Nov  6 11:32:54 2019

@author: hamza
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 12:33:08 2019

@author: hamza
"""
import numpy as np
import scipy.stats as sp
import matplotlib.pyplot as plt
from sklearn import datasets
import seaborn as sns
from scipy.stats import norm
iris = datasets.load_iris() # loading the data set
from scipy.stats import truncnorm


data1 = iris.data[:, 0] #sepal length 
data2 = iris.data[:, 1] #sepal width
data3 = iris.data[:, 2] #petal length
data4 = iris.data[:, 3] #petal width


#  function taken from the internet
def get_truncated_normal(mean=0, sd=1, low=0, upp=10):
    return truncnorm(
        (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)
    



#FOR TESTING NORAML DISTRIBUTION ON DATA

#SELECT DATA , data1,2,3,4
data = data3 #replace by which ever feature vector you want to fit


normdist = getattr(sp, 'norm')

# mean std
m,s = norm.fit(data)

#lower upper
l = min(data)
u = max(data)

X = get_truncated_normal(mean=m, sd=s, low=l, upp=u)

# 150 numbers from normal dist having m mean s std , l lower and u upper 
X = X.rvs(150)

# sample vs generated
fig, ax1 = plt.subplots()
ax1 = sns.distplot(X,color='g')
ax1 = sns.distplot(data,color='b')
plt.show()


# GOF CALCULATION
gof = max(abs(norm.cdf(data)-norm.cdf(X)))

# GOF PLOT
fig, ax2 = plt.subplots()
ax2 = sns.lineplot(data,normdist.cdf(data),c='r', label="Estimated")
ax2 =sns.lineplot(X,normdist.cdf(X),c='b',label = "True")
title = "Goodness-of-fit: gof = %.2f" % (gof)
plt.title(title,c='r')
plt.show()

