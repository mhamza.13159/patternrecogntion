# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 09:56:36 2019

@author: hamza
"""

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np


N = 1000
x = np.linspace(0,20,N)
density = np.random.normal(loc = 0.0, scale = 5.0, size = N)
y = (3*x) + density
df = pd.DataFrame({'y':y, 'x':x})
df['constant'] = 1

plt.figure()
plt.plot()
plt.plot(df.x, df.y,'g.' )
#plt.xlim(df.x)
#plt.ylim(df.y)
plt.title('Test figure')        
plt.show()

import matplotlib.pyplot as plt
fig = sns.regplot(df.x,df.y)
#ax.bar(x=['X','B','C'], height=[3.1,7,4.2], color='r')

fig, ax = plt.subplots()
ax.bar(x=df.x, height=df.y, color='r')
ax.set_xlabel(xlabel='X title', size=10)
ax.set_ylabel(ylabel='Y title' , color='b', size=10)
plt.show()